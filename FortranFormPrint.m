(* Package: FortranFormPrint *)
(* Description: An improved version of FortranForm function. *)
(* Version: 0.1.1 *)
(* Author: Maciej Maliborski, maciej.maliborski@gmail.com *)

BeginPackage["FortranFormPrint`"];


FortranFormPrint::usage = "FortranFormPrint[\!\(\*StyleBox[\"expr\", \"TI\"]\)] transforms expression \!\(\*StyleBox[\"expr\", \"TI\"]\)\!\(\*StyleBox[\" \", \"TI\"]\)to Fortran form with standards f77 or f90.";


Begin["`Priate`"]


Unprotect[FortranFormPrint];

Options[FortranFormPrint] = {"Standard" -> "f77", PageWidth -> Automatic};

FortranFormPrint::stand = "Unknown standard format `1`. Allowded standards are 'f77' or 'f90'.";

FortranFormPrint::ioppf = "Value of option PageWidth -> `1` should be a positive integer or Infinity.";


FortranFormPrint[expr_, OptionsPattern[]] :=
   Module[{std=OptionValue["Standard"], w=OptionValue["PageWidth"]/.{Automatic -> 72},
	   rules=((1/#[f_] :> 1/HoldForm[#[f]]) & /@ {Sin, Cos, Sinh, Cosh, Tan}), expr2},
	  expr2 = expr /. rules;
	  Switch[std,
		 "f77", fortranCellPrint77[expr2, w],
		 "f90", fortranCellPrint95[expr2, w],
		 _, Message[FortranFormPrint::stand, std]; fortranCellPrint95[expr2, w]
	  ]
   ];


fortranReplaceExp[str_String] := StringReplace[str, {Shortest["E**(" ~~ arg__ ~~ ")"] :> "Exp(" <> arg <> ")", Shortest["E**" ~~ x : WordCharacter ~~ y___ ~~ z : WhitespaceCharacter | EndOfString] :> "Exp(" <> x <> y <> ")" <> z}, IgnoreCase -> False];


fortranReplaceLineBreaks[str_String] := StringReplace[str, Shortest[StartOfLine ~~ l__ ~~ Except[EndOfString, EndOfLine]] :> l <> " &"] // StringReplace[#, StartOfLine ~~ "     -" :> "     &"] &;


fortranReplaceStartOfLine[str_String] := StringReplace[str, StartOfLine ~~ "     -" :> "     &"];
fortranReplaceEndOfLine[str_String] := StringReplace[str, Shortest[StartOfLine ~~ l__ ~~ Except[EndOfString, EndOfLine]] :> l <> " &"];


fortranForm[expr_, w_?(And[Or[IntegerQ[#], # == Infinity], Positive[#]]&)] := ToString[expr, FormatType -> FortranForm, PageWidth -> w] // fortranReplaceExp;
fortranForm[expr_, w_] := (Message[FortranFormPrint::ioppf, w]; Abort[];)


fortranCellPrint77[expr_, w_] :=
   CellPrint[Cell[fortranReplaceStartOfLine@fortranForm[expr, w], "Output"]];

fortranCellPrint95[expr_, w_] :=
   CellPrint[Cell[fortranReplaceEndOfLine@fortranReplaceStartOfLine@fortranForm[expr, w], "Output"]];


SetAttributes[FortranFormPrint, {Protected, ReadProtected}];


End[]


EndPackage[]
